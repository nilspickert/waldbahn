/********************
 * Automatisiertes Mini-Layout mit einer Weiche
 * Steuerung 0.1
 * 
 * Changelog
 * 05.11.2020 - Initiale Erstellung
 * 08.11.2020 - Umstellung auf PWM Erweiterungsboard
 * 11.11.2020 - Sound, kleineres Aufhübschen
 * 05.12.2020 - Unstieg auf Platform.io, sauberes gitlab Projekt mit Doku angefangen 
 * 06.12.2020 - Beleuchtung mit Neopixel-Streifen
 */

// ESP8266 Wifi stuff
#if defined(ESP8266)
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#else
#include <WiFi.h>
#endif

// Needed for OTA
#include <ESP8266mDNS.h>
#include <WiFiClient.h>

// WebServer and WifiManager
// WiFi Manager allows easy configuration of the ESPs WiFi, it opens a hotspot with captive portal if no WiFi can be found. 
#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h>         //https://github.com/alanswx/ESPAsyncWiFiManager

// OTA (over the air) Update function
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Hash.h>

// FastLED for Lighting with Neopixel RGB LED strip 
#include <FastLED.h>

// I/O Functions
#include <Wire.h>
// PCF8574 I/O expansion board
//#include <pcf8574_esp.h>
// PCA9685 PWM expansion board
#include <Adafruit_PWMServoDriver.h>

// Sound DFPlayer Mini
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"

//for LED status
#include <Ticker.h>
Ticker ticker;

// Pins used: SDA/SCL are i2c, INT is connected to INT pin on pcf board
#define PIN_INT D5
#define PIN_SDA D1
#define PIN_SCL D2

Adafruit_PWMServoDriver pwm1 = Adafruit_PWMServoDriver(0x40);

// Sound
SoftwareSerial mySerial(12,14); // RX on D5, TX on D6
DFRobotDFPlayerMini myDFPlayer;

// Pins for additional functions
#define PIN_OUT1 13 // D7 for relay

// Define Webserver
AsyncWebServer server(80); // for Captive Portal, is deactivated after WiFi connect
DNSServer dns;

// Define Neopixel Strip
#define NUM_LEDS 16 // the  strip I use has 3 leds on one controller -> divide actual number of LEDs by 3
#define DATA_PIN D3 

#define TEMPERATURE DirectSunlight
CRGB leds[NUM_LEDS];


// WiFi Manager Stuff
void tick()
{
  //toggle state
  int state = digitalRead(LED_BUILTIN);  // get the current state of GPIO1 pin
  digitalWrite(LED_BUILTIN, !state);     // set pin to the opposite state
}

//gets called when WiFiManager enters configuration mode
void configModeCallback (AsyncWiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  Serial.println(myWiFiManager->getConfigPortalSSID());
  //entered config mode, make led toggle faster
  ticker.attach(0.2, tick);
}

volatile bool PCFInterruptFlag = false;

void ICACHE_RAM_ATTR PCFInterrupt() {
  PCFInterruptFlag = true;
}
// END Wifi Manager Stuff


/* EISENBAHN FUNKTIONEN */
/* interval and speed need to be adapted to used engine and track length
*/
unsigned long previousMillis = 0;
unsigned long interval = 20000; // 20 sec for direction change
bool richtung = true;
int speed = 2200; // Range 0 to 1023, 350 seems to be a good value for slow movement with 12V input
int state = 0; // Statuszähler

void drive(bool direction) {
  if (direction == true) {
    // Direction Tunnel (might need change of polarity in your case...)
    pwm1.setPWM(0,1024,speed); // Channel 0 50% Duty cycle
    pwm1.setPWM(1,0,4096); // Channel 1 off
    digitalWrite(LED_BUILTIN, HIGH);
   } else {
    // Direction to Station(might need change of polarity in your case...)
    pwm1.setPWM(1,1024,speed); // Channel 1 50% Duty cycle
    pwm1.setPWM(0,0,4096); // Channel 0 off  
    digitalWrite(LED_BUILTIN, LOW); 
   }
}

void weiche(bool direction) {
  if(direction== true) {
    pwm1.setPWM(2,0,4096);
    pwm1.setPWM(3,4096,0);
  } else  {
    pwm1.setPWM(3,0,4096);
    pwm1.setPWM(2,4096,0);   
  } 
  delay(2000);
  pwm1.setPWM(2,0,4096);
  pwm1.setPWM(3,0,4096);
}

void entkuppeln() {
  pwm1.setPWM(4,0,4095);
  pwm1.setPWM(5,0,4096);
  delay(2000);
  pwm1.setPWM(4,0,4096);
}

void zugbfnachtunnel() {
  weiche(true); // Turn-Out straight through - Stelle Weiche auf gerade
  speed = 2200;
  myDFPlayer.play(2);
  drive(true); // Fahre richtung tunnel
}

void zugtunnelnachbf() {
  weiche(true);
  speed = 2200;
  myDFPlayer.loop(3);
  drive(false);
}

void lokbfnachschlacke() {
  digitalWrite(PIN_OUT1, LOW);
  myDFPlayer.play(2);
  weiche(false);  // Turn-out to switch to siding - Stelle Weiche auf Abzweig
  speed = 2200; 
  drive(true); 
  entkuppeln();
}

void lokwasserfassen_begin() {
  myDFPlayer.loop(4);
  drive(true);
  // TODO make slow
  pwm1.setPWM(8,1,3500);
  delay(1000);
  pwm1.setPWM(8,0,4906);
}

void idleschlacke() {
  myDFPlayer.loop(4);
  drive(true);
}

void lokwasserfassen_ende() {
  // TODO
  myDFPlayer.loop(4);
  // TODO make slow
  pwm1.setPWM(8,10,400);
  delay(1000);
  pwm1.setPWM(8,0,4096);
}


void lokschlackeschuppen() {
  myDFPlayer.play(2);
  digitalWrite(PIN_OUT1, HIGH);
  speed = 2200;
  drive(true);
}

void lokschuppennachbf() {
  myDFPlayer.play(2);
  digitalWrite(PIN_OUT1, LOW);
  speed = 2200;
  weiche(false);
  drive(false);
}

void idletunnel() {
 // nothing
 myDFPlayer.pause();
 drive(true);
}

void idlebahnhof() {
  // Geräusche Lok Idle
  myDFPlayer.loop(4);
  drive(false);
}

void idleschuppen() {
  // Geräusche Schuppen
  myDFPlayer.play(5);
  drive(true);
}

void setup() {

  Serial.begin(115200); // Serial console for debugging
  mySerial.begin(9600); // Serial connection to DFPlayer

  //set led pin as output
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(PIN_OUT1, OUTPUT);

  // start ticker with 0.5 because we start in AP mode and try to connect
  ticker.attach(0.6, tick);

  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  AsyncWiFiManager wifiManager(&server,&dns);

  //set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
  wifiManager.setAPCallback(configModeCallback);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect()) {
    Serial.println("failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(1000);
  }

  //if you get here you have connected to the WiFi
  Serial.println("connected...yeey :)");
  ticker.detach();
  //keep LED on
  digitalWrite(LED_BUILTIN, LOW);

   // Set Hostname for OTA Updates to identify ESP
  ArduinoOTA.setHostname("Waldbahn");
  
  // OTA Routines
  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_SPIFFS
      type = "filesystem";
    }

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });

  ArduinoOTA.begin();
  
  // Debug: send that we are ready and listening on an IP...
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());


  // Start the i2c interface
  Wire.begin(PIN_SDA, PIN_SCL);
  Wire.setClock(100000L);
  // Set up PWM
  pwm1.begin();
  pwm1.setPWMFreq(200); // Set PWM Frequency to 200Hz, to prevent high frequency noises from motors

  digitalWrite(PIN_OUT1, HIGH);

  // Licht anschalten
  // Strip has color ordring BRG, adapt for your strips
  FastLED.addLeds<WS2811, DATA_PIN, BRG>(leds,NUM_LEDS);
  //FastLED.setCorrection(TEMPERATURE);
  for(int whiteled = 0; whiteled < NUM_LEDS; whiteled++) {
    //leds[whiteled] = CRGB(255,255,210);
    leds[whiteled] = CRGB::Bisque;
    FastLED.show();
  }

  // MP3 Player intialisieren
  myDFPlayer.begin(mySerial);
  myDFPlayer.volume(8);
  myDFPlayer.play(1);

  // Zug in Bahnhof fahren (Annahme: Weiche steht noch unverändert und steht richtig)
  pwm1.setPWM(8,10,400);
  delay(1000);
  pwm1.setPWM(8,0,4096);

  drive(false);

  delay(15000); // wait 15 sec
  previousMillis = millis();
  myDFPlayer.volume(8); // set again, sometimes seems to miss
}

void loop() {
  // Handle any OTA request happening
  ArduinoOTA.handle();
  // put your main code here, to run repeatedly:
  if(millis() - previousMillis > interval) {
     previousMillis = millis();
     switch(state) {
      case 0: 
        zugbfnachtunnel();
        state = 1;
        interval = 25000; 
        break;
      case 1:
        idletunnel();
        state = 2;
        interval = 10000;
        break;
      case 2:
        zugtunnelnachbf();
        state = 3;
        interval = 16000;
        break;
      case 3:
        idlebahnhof();
        if(random(100)>50) {
        state = 4;
        } else {
          state = 10; 
        }
        interval = 20000;
        break;
      case 4:
        lokbfnachschlacke();
        state = 5;
        interval = 8000;
        break;
      case 5:
        idleschlacke();
        interval = 2000;
        state = 55;
        break;
      case 55:
        lokwasserfassen_begin();
        state = 6;
        interval = 7000;
        break;
      case 6:
        lokwasserfassen_ende();
        state = 66;
        interval = 2000;
        break;
      case 66:
        idleschlacke();
        if(random(100)>50) {
            state = 7;
          } else {
          state = 9; 
        }
        interval = 5000; // does nothing, just wating
        break;
      case 7:
        lokschlackeschuppen();
        state = 8;
        interval = 3000;
        break;
      case 8:
        idleschuppen();
        state = 9;
        interval = 30000;
        break; 
      case 9:
        lokschuppennachbf();
        state = 10;
        interval = 15000;
        break;
      case 10:
        idlebahnhof();
        state = 0;
        interval = 10000;
        break;
        
      default:
        drive(false); // Im Zweifelsfall zum Bahnhof fahren.    
     }
  }
     
}