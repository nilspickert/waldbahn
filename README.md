# Waldbahn v0.1

Beispiel für eine vollautomatisierte kleine Modelleisenbahn. Die Strecke ist ein einfaches Y mit einer Weiche und einem Entkuppelungsgleis. Der Zug fährt auf einer Strecke hin und her, die Lok wird ab und zu mal abgekuppelt und fährt über die Weiche zu einem kleinen Bahnbetriebswerk.

Die Modellbahn ist analog und verwendet Gleichstrom. Diese Schaltung funktioniert nicht mit Wechselstrom-Betrieb (zum Beispiel Märklin H0) und auch nicht mit Lokomotiven, die nur Digitalbetrieb können. Die verwendete Gleichspannung wird über Pulspreitenmodulation gesteuert, was auch bei einigen Gleichstrommotoren wohl zu Problemen führen kann. Analoge Spur N Lokomotiven (ohne Umbauten) von Minitrix, Fleischmann und Arnold haben bei mir funktioniert. Prinzipiell sollte es auch mit anderen Gleichstromsystemen gehen, sogar mit den alten Lego-Eisenbahnen.

## Benötigte Hardware

Die Links sind Beispiele von Materialien, die ich verwendet habe; natürlich kann man auch andere Materialien und Verkäufer nehmen. Bei Amazon habe ich Affiliate Links genommen, bei denen mir eine kleine Provision zukommt. Ihr könnt natürlich auch direkt bei Amazon suchen oder auch andere Verkäufer nehmen (insbesondere Modellbahnartikel sind meistens anderswo billiger, Elektronik ist bei AliExpress und anderen billiger, hat aber deutlich längere Versandzeiten). Wenn ihr ein lokales Modellbahngeschäft oder Elektronikgeschäft habt, kauft bitte alles dort - diese Läden sind am aussterben.

Der Bau meiner Anlage ist auf YouTube zu finden: https://www.youtube.com/playlist?list=PLwxe-0aWmdtk7_c1pdEysxoTaokkVk-cR


### Bahn
* [Schienen](https://amzn.to/3gaBJUK) für die Strecke
* Schienenverbinder [leitend](https://amzn.to/2VMxF3n) und [isoliert](https://amzn.to/33Iqo99)
* Eine [Weiche](https://www.dm-toys.de/de/produktdetails/Minitrix_14951.html) mit [elektromagnetischem Antrieb](https://www.dm-toys.de/de/produktdetails/Minitrix_14934.html)
* ein [Entkuppelungsgleis](https://amzn.to/36HaFsW)
* eine Lok (geeignet für Analogbetrieb) und einige Wagen

### Steuerung
* [Wemos D1 Mini](https://amzn.to/3mOfl5S) als Steuerzentrale
* [2x L298N H-Brücke](https://amzn.to/3lJn8kh) zur Stromversorgung des Zugs und der Magnetartikel
* [PCA9685 i2c PWM Erweiterungsmodul](https://amzn.to/36Gr54R) um mehr Outputs zu haben
* [DFPlayer Mini](https://amzn.to/2JCjLi0) für die Geräusche (benötigt noch eine Micro-SD Karte)
* [Lautsprecher](https://amzn.to/3ggY6YK)
* Kupferlitzen
* [12V Netzteil](https://amzn.to/2L1IDzY) plus Kabel/Stecker
* [12V zu 5V Konverter](https://amzn.to/3gakkeQ)
* [Dioden](https://amzn.to/3lIKaYr) (Typ ist relativ egal, sollten 12V und etwa 2A abkönnen)
* NPN Transistor (z.B. aus einem [Sammelpack](https://amzn.to/39OkZRV))
* [Relais](https://amzn.to/33H8XWL)
* Widerstand (z.B. aus einem [Sammelpack](https://amzn.to/2IfS7GZ))
* Lochrasterplatinen (z.B. im [Pack mit Kabelklemmen](https://amzn.to/2JQ5Cxo))
* Kabelklemmen für Platinen (s.o.)
* [Jumper-Wire mit Dupont-Stecker](https://amzn.to/2JLYPou)

## Streckenplan
Meine Anlage soll in ein Ikea-Regal passen, daher ist die Grundfläche auf 800mm x 300mm beschränkt. Grundplatte ist 6mm Sperrholz mit 40mm x 10mm Holzleisten (hochkant) darunter um Raum für die Elektronik zu schaffen. Die Fläche ist ausreichend für einen Zug in Spur N aus einer Länderbahn Lok mit drei Länderbahn-Wagen. Für längere Fahrzeuge oder größere Maßstäbe ist eine größere Grundfläche nötig.

![Gleisplan der Anlage, Y aus Schienen](docu/minilayout_ardu.jpg "Gleisplan der Anlage, Y aus Schienen")

Die Strecke ist in mehrere elektrische Abschnitte eingeteilt. An den Rot markierten Stellen ist jeweils eine Schiene durchgeschnitten und mit isolierenden Schienenverbindern wieder zusammen gesteckt. Für den Abschnitt A ist das die im Bild untere Schiene, für die anderen Abschnitte die im Bild oberen Schienen (wenn man in das Gleis hineinfährt, in Fahrtrichtung jeweils rechts).

![Elektrische Abschnitte](docu/minilayout_abschnitte.jpg "Elektrische Abschnitte")

An den Stellen A bis D im Bild wird jeweils an die getrennte Schiene ein Versorgungskabel angelötet, an der Stelle E (das ist die "Hauptstrecke") weden an beide Schienen die Versorgungskabel angelötet. Es bietet sich an, hier konsistente Farben für die Kabel zu verwenden, zum Beispiel für alle Schienen auf der im Bild unteren Seite Rot, für alle im Bild oben Schwarz).

Die Abschnitte A bis D sind "Einbahnstraßen", die Lok kann dort nur herausfahren. Die Lok fährt also solange dort hinein, bis alle stromabnehmenden Räder im Abschnitt sind. Dort bleibt sie dann stehen, bis die Versorgungsspannung umgepolt wird, und fährt dann wieder hinaus. Die Schaltung dazu ist sehr einfach, es ist eine einfache Diode, die dort eingelötet wird.

![Elektrische Einbahnstraße mit Diode](docu/einbahn.png "Schema der elektrischen Einbahnstraße")

Um das ganze etwas übersichtlicher zu machen, habe ich die Dioden nicht direkt an die Schienen gelötet, sondern an die Versorgungskabel. Dazu habe ich eine Lochrasterplatine mit dem folgenden Aufbau gebaut, an die die Kabel mit Schraubklemmen angeschlossen werden können. Der Abschnitt C ist hier etwas anders: da ich die Lok auch manchmal in den Abschnitt D fahren lassen will (Lokschuppen), muss hier die Diode überbrückt werden können. Dies geschieht mit einem Relais, zu dem wir später noch kommen. Auf der Platine habe ich dazu einfach einen Stecker eingebaut, mit denen das Relais dann angeschlossen werden kann, der aber auch zum Testen einfach mit einem Jumper überbrückt werden kann.

![Schaltplan Dioden in der Anschlussleitung](docu/dioden_waldbahn_Schaltplan.jpg "Schaltplan der Dioden in der Anschlussleitung")

## Steuerung (Aufbau)
### Stromversorgung
Die Stromversorgung geschieht über zwei verschiedene Spannungen: 12V für Modellbahn, 5V für den ESP und die Steuerungselektronik. Dazu wird der 5V Konverter an das 12V Netztteil angeschlossen. GND (der "Minus-Pol") ist für beide gleich und werden zusammen geschlossen. Die positiven Spannungen werden dann an die Bauteile geleitet und an die jeweiligen VCC/V+ Anschlüsse angeschlossen.

### Schaltplan
* Der D1 wird an +5V und GND angeschlossen mit Pins 5V und G.
* Das PCA9685 Modul wird mit VCC an den D1 Pin 3V3 angeschlossen (3V Spannung für die Logik), mit V+ an 5V des Spannungskonverter, GND an GND, SCL an Pin D2 und SDA an Pin D1 des Wemos D1.
* Ein L298N wird an mit In1 und In2 an PWM0 und PWM1 des PCA9685 angeschlossen. V_IN geht an die 12V des Netztteils, GND an den gemeinsamen GND. Die Motorausgänge gehen dann an Gleis 1/2 der Dioden-Schaltung für die Gleisversorgung
* Der zweite L298N wird mit In1/2 an PWM2/3 und mit In3/4 an PWM4/5 angeschlossen. Motorausgang 1 (der mit In1/2 gesteuert wird) geht dann an die Weiche, hier mit Dioden umd beide Steuerungen der Weiche mit einfacher Umpolung zu erreichen - die Polung sollte noch kontrolliert werden, wenn die SW läuft, eventuell ist hier Umpolen nötig, wenn die Weiche in die falsche Richtung schaltet. Motorausgang 2 an das Entkupplungsgleis (Polung ist hier egal).
* Das Relais wird über einen NPN Transistor angesteuert, da der Output Pin des Wemos D1 nicht genügend Strom liefern kann. Dazu wird die Basis des Transistors an Pin D7 mit einem 1kOhm Widerstand angeschlossen, der Emitter an GND und der Kollektor an das Relais. Die Spule des Relais wird noch mit einer Diode in Sperrrichtung überbrückt, um Spannungsspitzen beim Schalten abzufangen. Der zweite Anschluss der Relais-Spule geht dann an die passende Versorgungsspannung - in meinem Falle 12V, da ich ein 12V Relais genommen habe. Natürlich kann man hier auch 5V Relais nehmen, wenn welche zur Hand sind. An das Relais wird dann die Diode D4 aus der Schienen-Versorgung an den NO (normally open) Ausgang angeschlossen.

![Schaltplan ESP](docu/waldbahn_esp_Schaltplan.jpg "Schaltplan der Steuerung ohne Sound")

Der DFPlayer (nicht im Schaltplan eingezeichnet) wird dann noch einfach über eine serielle Verbindung angeschlossen, siehe auch https://wiki.dfrobot.com/DFPlayer_Mini_SKU_DFR0299

VCC des DFPlayer geht an +5V, GND an die gemeinsame Erde GND. SPK_1 und SPK_2 werden mit dem Lautsprecher verbunden. RX geht an Pin D5, TX an Pin D6 des Wemos D1. In den DFPlayer muss noch eine Micro-SD Karte gesteckt werden, auf die in dieser Reihenfolge die folgenden MP3s kopiert werden (passende MP3s einfach im Netz suchen):

1. Startup-Sound
2. Geräusch Zug fährt los 
3. Zug in Fahrt
4. Zug steht, Pumpengeräusch oder ähnliches
5. Zug lässt Dampf ab, Lok steht


## Steuerung (SW)
Um die SW zu verwenden, entweder als ZIP herunterladen (Download Symbol oben) oder per 'git clone "https://gitlab.com/nilspickert/waldbahn.git"' aus dem Repository kopieren. 

Die SW kann über zwei verschiedene IDEs auf den Wemos D1 kopiert werden, die normale [Arduino IDE](https://www.arduino.cc/en/software) oder [PlatformIO](https://platformio.org/). PlatformIO ist etwas mächtiger und eleganter (installiert zum Beispiel benötigte Bibliotheken automatisch), daher würde ich mittlerweile diesen Weg empfehlen (in den YouTube Videos ist noch die ArduinoIDE zu sehen).

### Installation Platform.io
Platform.IO ist eine Erweiterung für Microsoft Visual Studio Code. Sowohl Platform.IO als auch Visual Studio Code sind für alle gängigen Betriebssysteme erhältlich. Die Installation und Einrichtung ist recht einfach:
1. [Visual Studio Code](https://code.visualstudio.com/) herunterladen und installieren
2. Visual Studio Code starten und die Platform.IO Erweiterung installieren: Crtl-Shift-X drücken, PlatformIO in die Suchleiste tippen, auf Install klicken
3. Die nötige ESP Platform installieren: auf das PlatformIO Icon links in der Leiste klicken (das kleine Alien-Gesicht recht weit unten), dann in dem erscheindenden "Quick Access Menu" auf Platforms gehen, dort dann in der Suchleiste "Espressiff8266" suchen ("ESP" sollte schon reichen, um es anzuzeigen). Auf Installieren klicken

### Kompilieren und hochladen
In Visual Studio Code dann das SW Projekt öffnen: auf das PlatformIO Icon klicken, im Quick Access Menu dann auf "Open" und das Verzeichnis mit der heruntergeladenen SW öffnen.

Den Wemos D1 mit einem USB Kabel an den Rechner anschließen (im Idealfall sollte der D1 noch nicht an die Anlage oder eine externe Stromversorgung angeschlossen sein, um Kurzschlüsse oder ähnliches zu vermeiden). Im Menu "Poject Tasks" (oberhalb des Quick Access Menu) das Untermenu "Default" aufmachen, dann dort auf "Build" klicken, abwarten, dass die Kompilierung durchläuft. Dann auf "Upload" klicken. PlatformIO sollte automatisch den USB Port erkennen und die SW auf den D1 hochladen.

Der Wemos D1 sollte dann kurz blau blinken und sich neu starten. Damit die SW später einfacher aktualisiert werden kann, kann er auch "Over the Air" aktualisiert werden, nachdem die SW zum ersten mal installiert wurde. Dazu spannt der Wemos D1 erst einmal einen Access Point auf. Mit einem Handy oder einem Computer dann nach einem WLAN namens "ESP..." suchen und sich dort einwählen (kein Passwort nötig). In diesem WLAN wird dann eine Anmelde-Seite angezeigt, in der man den Wemos D1 dann in das lokale Heimnetz einwählen kann - dazu das lokale WiFi auswählen, Passwort angeben und der Wemos startet dann neu und wählt sich in dieses Netz ein. Wenn der ESP kein bekanntes Netz findet, geht er automatisch wieder in den AccessPoint Modus und man kann weitere Netze hinzufügen.

Der Wemos D1 ist dann im lokalen Netz sichtbar und kann über die ID "Waldbahn" dann auch per WiFi aktualisiert werden. Dazu dann bei Project Tasks in das Untermenu "env:d1_mini_OTA" gehen, und dort dann Build bwz Upload auswählen. Derzeit ist dies noch nicht geschützt, was zuhause kein Problem ist, aber auf einer Modellbahnausstellung oder anderen öffentlichen Netzen geändert werden sollte.

